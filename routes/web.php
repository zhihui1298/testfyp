<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FileController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ExcelController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// });

Route::get('/login', [HomeController::class, 'fake'])->name('fake');

Route::get('/home', [HomeController::class, 'home'])->name('home');

Route::post('/upload-file', [FileController::class, 'fileUpload'])->name('fileUpload');

Route::post('/exportMerge', [ExcelController::class, 'exportMerge'])->name('exportMerge');

Route::prefix('{file}')->group(function () {
    Route::get('/', [ExcelController::class, 'show'])->name('show');
    Route::get('/export', [ExcelController::class, 'export'])->name('export');
});
