<?php

namespace App\Http\Controllers;

use App\Models\File;


class HomeController extends Controller
{
    public function home()
    {
        $files = File::all();

        return view('home', compact('files'));
    }

    public function fake()
    {
        // $files = File::all();
        return view('fake');
    }
}
