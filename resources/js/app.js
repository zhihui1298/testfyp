require('./bootstrap');

$( function() {
    $('#uploadFileButton').click( function() {
        $('#upload-file').removeClass('hidden');
        // alert('wtf');
    })
    
    $('#cancelUpload').click( function() {
        $('#upload-file').addClass('hidden');
        // alert('wtf');
    })

    $('#input-upload-file').bind('change', function() { 
        var fileName = '';
        fileName = $(this).val();
        $('#file-selected').html(fileName);
    })

    $('input[type="checkbox"]').click( function() {
        var bool = false;
        var xs = document.querySelectorAll('input[type="checkbox"]');
        xs.forEach((x) => {
            if($(x).is(":checked")) {
                bool = true;
            }
        })

        if(bool) {
            $('#merge-button').removeClass('hidden');
        } else {
            $('#merge-button').addClass('hidden');
        }
    })

    $("#selectAll").click(function () {
        $('input:checkbox').not(this).prop('checked', this.checked);     
    });
})