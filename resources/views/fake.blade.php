<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" integrity="sha512-+4zCK9k+qNFUR5X+cKL9EIR+ZOhtIloNl9GIKS57V1MyNsYpYcUrUeQc9vNfzsWfV28IaLL3i96P9sdNyeRssA==" crossorigin="anonymous">
    <title>Login</title>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" defer></script>
    <script src="{{ mix('js/app.js') }}" defer></script>
    </head>
    <body>
        <div class="font-sans text-gray-900 antialiased">
            <div class="min-h-screen flex flex-col sm:justify-center items-center pt-6 sm:pt-0 bg-background">
    <div>
        <span class="text-5xl text-white"></span>
    </div>

    <div class="w-full sm:max-w-md mt-6 px-6 py-4 bg-gray-800 shadow-md overflow-hidden sm:rounded-lg">
        <form method="POST" action="http://localhost:8090/login">
            <input type="hidden" name="_token" value="Q66EtQ56Oi5vrs4Mvbrmf8AY6EmBsxX846D4lbAU">
            <div>
                 <label class="block font-medium text-sm text-white" for="email">
    Username
</label>
 
                 <input class="form-input rounded-md shadow-sm block mt-1 w-full" id="email" type="email" name="email" autofocus="autofocus">
 
            </div>

            <div class="mt-4">
                 <label class="block font-medium text-sm text-white" for="password">
    Password
</label>
 
                 <input class="form-input rounded-md shadow-sm block mt-1 w-full" id="password" type="password" name="password" autocomplete="current-password">
 
            </div>



            <div class="flex items-center justify-end mt-4">
                
                 <a href="{{ route('home') }}" class="inline-flex items-center px-4 py-2 bg-white border border-transparent rounded-md font-semibold text-xs text-black uppercase tracking-widest hover:bg-gray-399 active:bg-gray-300 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150 ml-4">
                     
    Login
                 </a>
 
            </div>
        </form>
    </div>
</div>
        </div>
    

 
</body></html>